import math
import multiprocessing as mp
import time

from functools import partial
import matplotlib.pyplot as plt
import numpy as np
from numpy import sqrt, exp, pi, double
import pandas as pd
from scipy.integrate import simps

#-- Macroparameters --
STEP = 0.5 #  
# STEP = 0.2 # takes forever
CUTOFF_DIFFERENCE = 0.1

ZEND = 10 # 23
z_init = np.arange(-ZEND, ZEND, STEP, dtype=np.double)
gamma_opt_init = 0.35
gamma_acu_init = 0.035
qb = 1
POLARON_SHIFT=True 
# POLARON_SHIFT=False
Case=[{"Name" : "QD/PIB",
      "FileName" : "QD_PIB",
      "Temperatures" : [4,10,20,40,77,150,300], # Kelvin
      "SHR" : 1.31,
      "Elo" : 21.0773, # mEv
      "Eg" : 2.1*1e3,  # mEv
      }, 
      {"Name" : "QD/Glass",
      "FileName" : "QD_Glass",
      "Temperatures" : [4,10,20,40,77,150,300], # Kelvin
      "SHR" : 1.67,
      "Elo" : 21.3253, # mEv
      "Eg" : 2.09*1e3, # mEv
      },
      {"Name" : "QD/Toluol",
      "FileName" : "QD_Toluol",
      "Temperatures" : [4,10,20,40,77,150], # Kelvin
      "SHR" : 2.66,
      "Elo" : 22.6891, # mEv
      "Eg" : 2.07*1e3, # mEv
      }]
w_opt = lambda Elo, qx, qy, qz: Elo + (np.cos(qx) + np.cos(qy) + np.cos(qz))/3 -1
w_acu = lambda Max, qx, qy, qz: 0.1-Max*(np.cos(qx*0.5+np.pi/2)+np.cos(qy*0.5+np.pi/2)+np.cos(qz*0.5+np.pi/2)) #0.1 added so it starts not from 0

def Spectrum(SE, gamma, E, z): 
    ans = [ abs(( -1j*gamma/( z[i]-SE[i]+1j*gamma ) )**2) for i in range(len(z))]
    maximum = np.amax(ans)
    return ans/maximum

def C(perturbative, acoustic, Shr, gamma, E, T, q, z): # поларонный сдвиг и у Сз и у лямбды, центр Сз такой же как у спектра, в нуле
    def Lambda(w,z):
        def func1(qx1, qy1, qz1, w, z): 
            if acoustic:
                w1 = w_acu(E,qx1,qy1,qz1)
                w1_min = w_acu(E,0,qy1,qz1) # чтобы катоф работал, приходил к 0 в конце
            else:
                w1 = w_opt(E,qx1,qy1,qz1)
                w1_min = w_opt(E,np.pi,qy1,qz1) # чтобы катоф работал, приходил к 0 в конце
            
            cutoff = (w1-w1_min)**3*exp(-(w1-w1_min)**2/2)
            return Shr*cutoff*(  
            +(1+(exp(w1/T)-1.0)**-1)/((z-w1+1j*gamma)*(z-w1-w+1j*gamma)) 
            +   (exp(w1/T)-1.0)**-1 /((z+w1+1j*gamma)*(z+w1+w+1j*gamma))
            -(1+(exp(w1/T)-1.0)**-1)/ (z-w1+1j*gamma)**2 
            -   (exp(w1/T)-1.0)**-1 / (z+w1+1j*gamma)**2 )
        
        qlen = len(q)
        Intz1 =  np.zeros(qlen, dtype=complex)
        for ii, qz1 in enumerate(q):
            Inty1 = np.zeros(qlen, dtype=complex)
            for jj, qy1 in enumerate(q):
                Intx1 = [func1(qx1, qy1, qz1, w, z) for qx1 in q]
                Inty1[jj] = simps(Intx1, q)
            Intz1[ii] = simps(Inty1,q)
        A = simps(Intz1,q)
        return A
    
    def func(qx, qy, qz, z):
        if acoustic:
            w = w_acu(E,qx,qy,qz)
            w_min = w_acu(E,0,qy,qz) # чтобы катоф работал, приходил к 0 в конце
            middle=0 # middle of the graph
        else:
            w = w_opt(E,qx,qy,qz)
            w_min = w_opt(E,np.pi,qy,qz) # чтобы катоф работал, приходил к 0 в конце
            middle=E
        if perturbative:
            Lambda_pol=1
            Lambda_nopol=1
        else:
            Lambda_pol=(1+(Lambda(w,z) - Lambda(w,middle)))**2
            Lambda_nopol=(1+(Lambda(w,z) ))**2  
        cutoff = (w-w_min)**3*exp(-(w-w_min)**2/2) # w+diff чтобы w-E не были отрицательными, иначе поломается катоф. В остальных местах не нужно diff
        if POLARON_SHIFT:
            # return Lambda_pol
            return Shr*cutoff*(( 
            +(1+ (exp(w/T)-1.0)**-1) / (z-w+1j*gamma)  
            +   ((exp(w/T)-1.0)**-1) / (z+w+1j*gamma) )
            -( 
            +(1+ (exp(w/T)-1.0)**-1) / (middle-w+1j*gamma) 
            +   ((exp(w/T)-1.0)**-1) / (middle+w+1j*gamma) ))*Lambda_pol
        else:
            # return Lambda_nopol
            return Shr*cutoff*(( 
            +(1+ (exp(w/T)-1.0)**-1) / (z-w+1j*gamma) 
            +   ((exp(w/T)-1.0)**-1) / (z+w+1j*gamma) )
            )*Lambda_nopol
    
    qlen = len(q)
    Intz =  np.zeros(qlen, dtype=complex)
    for i, qz in enumerate(q):
        Inty = np.zeros(qlen, dtype=complex)
        for j, qy in enumerate(q):
            Intx = [func(qx, qy, qz, z) for qx in q]            
            Inty[j] = simps(Intx, q)
            
        Intz[i] = simps(Inty,q)
    return simps(Intz,q)

def parallel(func, perturbative, acoustic, Shr, gamma, E, T, q, z): 
    part = partial(func, perturbative, acoustic, Shr, gamma, E, T, q)
    if __name__ == '__main__': 
        p = m.Pool()
        ans = p.map(part, z)
        
    return np.array(ans)

def All():
    qq = np.arange(0,np.pi,STEP) 
    start_time = time.time()
    Ela = 9 # max energy, from 0 to 13

    for i in range(len(Case)):
        T_val = np.multiply(Case[i]["Temperatures"],0.0862) # in mEv
        Shr = Case[i]["SHR"]
        Elo = Case[i]["Elo"]
        Eg = Case[i]["Eg"]
        Name = Case[i]["Name"]
        
        col_names = ["z_acu", "z_acu_cm", "Cz_acu_Re", "Cz_acu_Im", "Spec_acu", "Cz_acu_Pert_Re", "Cz_acu_Pert_Im", "Pert_Spec_acu",
                     "z_opt", "z_opt_cm", "Cz_opt_Re", "Cz_opt_Im", "Spec_opt", "Cz_opt_Pert_Re", "Cz_opt_Pert_Im", "Pert_Spec_opt"]
        
        for T in T_val:
            gamma_opt = gamma_opt_init + (T - T_val[0])*(gamma_opt_init)/(T_val[-1]-T_val[0])
            gamma_acu = gamma_acu_init + (T - T_val[0])*(gamma_acu_init)/(T_val[-1]-T_val[0])
            # Exo = Eg - 2*Shr*Elo/(np.exp(Elo/T)-1) # Exo то полная энергия экситона, порядка 2 эВ. Мы отстраиваемся от нее, z-Exo
            z_acu = z_init
            z_acu_cm = -z_acu*8.05
            z_opt = z_init + Elo
            z_opt_cm = -z_opt*8.05
            # Non-perturbative
            CAcu_ans = parallel(C, False, True, Shr, gamma_acu, Ela, T, qq, z_acu)
            COpt_ans = parallel(C, False, False, Shr, gamma_opt, Elo, T, qq, z_opt)
            plt.plot(np.real(COpt_ans))
            plt.plot(np.real(Ans3))
            
            plt.show()
            
            
            plt.plot(np.imag(COpt_ans))
            plt.plot(np.imag(Ans3))
            plt.show()
            
            raise SystemExit(0)
            SA = Spectrum(CAcu_ans, gamma_acu, Ela, z_init)
            SO = Spectrum(COpt_ans, gamma_opt, Elo, z_init)
            # Perturbative
            CAcuPert_ans = parallel(C, True, True, Shr, gamma_acu, Ela, T, qq, z_acu)
            COptPert_ans = parallel(C, True, False, Shr, gamma_opt, Elo, T, qq, z_opt) 
            SAPert = Spectrum(CAcuPert_ans, gamma_acu, Ela, z_init)
            SOPert = Spectrum(COptPert_ans, gamma_opt, Elo, z_init)
            
            df = pd.DataFrame(np.column_stack([z_acu, z_acu_cm, np.real(CAcu_ans), np.imag(CAcu_ans), SA, np.real(CAcuPert_ans), np.imag(CAcuPert_ans), SAPert,
                                               z_opt, z_opt_cm, np.real(COpt_ans), np.imag(COpt_ans), SO, np.real(COptPert_ans), np.imag(COptPert_ans), SOPert]), columns=col_names)
            df.to_csv(Case[i]["FileName"]+str(math.ceil(T/0.0862))+'K.csv', index=False)

    print("--- %s seconds ---" % (time.time() - start_time))
    return None
        
m = mp.Manager()
All()