#include <iostream>
#include <complex>
#include <fstream>
#include <vector>
#include <pthread.h>
#include <algorithm>
#include <string>

#include <future>
	 

#define  II << ' ' <<



// Create imaginary type
typedef std::complex<double> dcomp;

namespace workspace {

const dcomp I = sqrt(dcomp(-1));
const double Pi = 3.14159, gamma=0.077;
// const double STEP = 0.1; // 32 days 
// const double STEP = 0.2; // 74 minutes (1.25 hours)
const double STEP = 0.5; // 7 sec
// const double STEP = 1; // 16 min
const int THREADS = 8;
// const int THREADS = 6;
const int CUTOFF_DIFFERENCE = 0.1;
// const int CUTOFF_DIFFERENCE = 1;


const double gamma_opt_init = 0.35;
const double gamma_acu_init = 0.035;

const int ZSTART= -30, ZEND = 30 ; //ZEND = 10
const int Z_LEN = (ZEND - ZSTART)/STEP +1; // +1 is to use < instead of <=, for optimizations

const double QSTART= 0, QEND = Pi ;
const int Q_LEN = (QEND - QSTART)/STEP +1; // +1 is to use < instead of <=, for optimizations

const int TEMP_LEN = 4;
const int CASES = 4;
const int SPECTRES = 2;
const int Temperatures[TEMP_LEN] = {4, 40, 150, 300}; // {4, 10, 20, 40, 77, 150, 300};

// const double EG[3] = {2.1*1e3, 2.09*1e3, 2.07*1e3}; //mEv
int queue_pos;
dcomp C_Ans[Z_LEN][TEMP_LEN][CASES];
std::future<dcomp> C_Ans_handle[Z_LEN][TEMP_LEN][CASES];
double CSpec[Z_LEN][TEMP_LEN][SPECTRES];

double w_opt(double E, double qx, double qy, double qz){ return E + (cos(qx) + cos(qy) + cos(qz))/3.0 -1.0;};
double w_acu(double E, double qx, double qy, double qz){ return 0.01+E*(sin(qx*0.5)+sin(qy*0.5)+sin(qz*0.5))/3.0;}; //0.1 added so it starts not from 0 0.1
// double w_acu(double E, double qx, double qy, double qz){ return 0.01+E*(sin(qx*0.5)+sin(qy*0.5)+sin(qz*0.5));}; //0.1 added so it starts not from 0 0.1


dcomp simpsons_(dcomp fx[Q_LEN])
{
	dcomp res = 0;

    for (int i = 0; i < Q_LEN; i++) {
        if (i == 0 || i == Q_LEN-1)
            res += fx[i];
        else if (i % 2 != 0)
            res += 4.0 * fx[i];
        else
            res += 2.0 * fx[i];
    }
    
    res = res * (STEP / 3.0);
    return res;
}

dcomp Lam(bool perturbative, bool polaron_shift, bool acoustic, double Shr, double cutoff, double w1, double T, double gamma, double E, double w, double z)
{

	return Shr*CUTOFF_DIFFERENCE*cutoff*(  
	(
    +(1+pow(exp(w1/T)-1.0,-1.0))/((z-w1+I*gamma)*(z-w1-w+I*gamma)) 
    +   pow(exp(w1/T)-1.0,-1.0) /((z+w1+I*gamma)*(z+w1+w+I*gamma))
    -(1+pow(exp(w1/T)-1.0,-1.0))/ pow(z-w1+I*gamma,2.0) 
    -   pow(exp(w1/T)-1.0,-1.0) / pow(z+w1+I*gamma,2.0)
    )

    );
}


dcomp Integ3d(bool perturbative, bool polaron_shift, bool acoustic, dcomp(*func)(bool, bool, bool, double, double, double, double, double, double, double, double), double Shr, double gamma, double E, double T, double w_out, double z)
{
    dcomp Intz[Q_LEN];
    double qz,qy,qx;
    for (int i = 0; i < Q_LEN; ++i)
    {
    	double qz = QSTART+i*STEP;
    	dcomp Inty[Q_LEN];
    	for (int j = 0; j < Q_LEN; ++j)
    	{
    		double qy = QSTART+j*STEP;
    		dcomp Intx[Q_LEN];
    		for (int k = 0; k < Q_LEN; ++k)
    		{
    			double qx = QSTART+k*STEP;
    			double w, w_min, cutoff;
    			if (acoustic){
					w = w_acu(E,qx,qy,qz);
					w_min = w_acu(E,0.0,qy,qz);// чтобы катоф работал, приходил к 0 в конце
					cutoff = pow(w,3.0)*exp(-pow(w,2.0)/2.0);
					// w = w_opt(E,qx,qy,qz);
					// w_min = w_opt(E,Pi,qy,qz);// чтобы катоф работал, приходил к 0 в конце
				} else {
					w = w_opt(E,qx,qy,qz);
					w_min = w_opt(E,Pi,qy,qz);// чтобы катоф работал, приходил к 0 в конце
					cutoff = pow(w-w_min,3.0)*exp(-pow(w-w_min,2.0)/2.0);
				}
				// cutoff = pow(w-w_min,3.0)*exp(-pow(w-w_min,2.0)/2.0);
				// double cutoff = 0.1;
				Intx[k] = func(perturbative, polaron_shift, acoustic, Shr, cutoff, w, T, gamma, E, w_out, z);
			}
    		Inty[j] = simpsons_(Intx);
    	}
    	Intz[i] = simpsons_(Inty);
    }
    return simpsons_(Intz);
}

dcomp Cfun(bool perturbative, bool polaron_shift, bool acoustic, double Shr, double cutoff, double w, double T, double gamma, double E, double w_out, double z)
{
	dcomp Lambda_pol, Lambda_nopol;
	double middle;
	if (acoustic){
		middle = 0.0;
	} else {
		middle = E;
	}
	if (perturbative){
        Lambda_pol=1.0;
        Lambda_nopol=1.0;
    } else if (polaron_shift){
    	Lambda_pol = pow(1.0+Integ3d(perturbative, polaron_shift, acoustic, Lam, Shr, gamma, E, T, w, z)-Integ3d(perturbative, polaron_shift, acoustic, Lam, Shr, gamma, E, T, w, middle),2.0);
    }else{
        Lambda_nopol = pow(1.0+Integ3d(perturbative, polaron_shift, acoustic, Lam, Shr, gamma, E, T, w, z),2.0);
    }
	if (polaron_shift){
		// return Lambda_pol;
        return 	Shr*cutoff*(( 
        +(1+pow(exp(w/T)-1.0,-1.0)) / (z-w+I*gamma)  
        +   pow(exp(w/T)-1.0,-1.0) / (z+w+I*gamma) )
        -( 
        +(1+pow(exp(w/T)-1.0,-1.0)) / (middle-w+I*gamma) 
        +   pow(exp(w/T)-1.0,-1.0) / (middle+w+I*gamma) ))*Lambda_pol;
    } else {
    	// return Lambda_nopol;
        return Shr*cutoff*(( 
        +(1+ pow(exp(w/T)-1.0,-1.0)) / (z-w+I*gamma) 
        +  (pow(exp(w/T)-1.0,-1.0)) / (z+w+I*gamma) )
        )*Lambda_nopol;
    }
}


void ParC(double Shr, double Ela, double Elo)
{
	double max_element1,max_element2;
	double T,T0,Tinf,gamma_opt,gamma_acu,z;

    for (int j = 0; j < TEMP_LEN; ++j) // for TEMP_LEN temperatures
	{
    	
    	T = Temperatures[j]*0.0862, T0 = Temperatures[0]*0.0862, Tinf = Temperatures[TEMP_LEN-1]*0.0862;
        gamma_opt = gamma_opt_init + (T-T0)*(gamma_opt_init)/(Tinf-T0);
        gamma_acu = gamma_acu_init + (T-T0)*(gamma_acu_init)/(Tinf-T0);
        max_element1 = 0;
        max_element2 = 0;

    	for (int i = 0; i < Z_LEN; i++) //perturbative, polaron_shift, acoustic
		{
	    	z = ZSTART + i*STEP;
			
			C_Ans_handle[i][j][0] = std::async(std::launch::async, Integ3d, true,  true, false, Cfun, Shr, gamma_opt, Elo, T, 0, z+Elo);
			C_Ans_handle[i][j][1] = std::async(std::launch::async, Integ3d, false, true, false, Cfun, Shr, gamma_opt, Elo, T, 0, z+Elo);
			C_Ans_handle[i][j][2] = std::async(std::launch::async, Integ3d, true,  true, true, Cfun, Shr, gamma_acu, Ela, T, 0, z);
			C_Ans_handle[i][j][3] = std::async(std::launch::async, Integ3d, false, true, true, Cfun, Shr, gamma_acu, Ela, T, 0, z);

			// C_Ans[i][j][0] = Integ3d(true,  false, false, Cfun, Shr, gamma_opt, Elo, T, 0, z+Elo);
			// C_Ans[i][j][1] = Integ3d(false, false, false, Cfun, Shr, gamma_opt, Elo, T, 0, z+Elo);
			// C_Ans[i][j][2] = Integ3d(true,  false, true, Cfun, Shr, gamma_acu, Ela, T, 0, z);
			// C_Ans[i][j][3] = Integ3d(false, false, true, Cfun, Shr, gamma_acu, Ela, T, 0, z);
		}
		for (int i = 0; i < Z_LEN; i++) //perturbative, polaron_shift, acoustic
		{
			z = ZSTART + i*STEP;

			C_Ans[i][j][0] = C_Ans_handle[i][j][0].get();
			C_Ans[i][j][1] = C_Ans_handle[i][j][1].get();
			C_Ans[i][j][2] = C_Ans_handle[i][j][2].get();
			C_Ans[i][j][3] = C_Ans_handle[i][j][3].get();
            

	        CSpec[i][j][0] = pow(std::abs( -I*gamma_opt/( z-C_Ans[i][j][0]+I*gamma_opt) 
	        	                           -I*gamma_acu/( z+(Elo)-C_Ans[i][j][2]+I*gamma_acu)
	        	                           ),2.0);
			CSpec[i][j][1] = pow(std::abs( -I*gamma_opt/( z-C_Ans[i][j][1]+I*gamma_opt)  
				                           -I*gamma_acu/( z+(Elo)-C_Ans[i][j][3]+I*gamma_acu)
				                           ),2.0);// I need to shift acoustic ones, but not straghtforward ыолны не от сдвига

			if (CSpec[i][j][0] > max_element1){ max_element1 = CSpec[i][j][0];} // finding bigest element for normalization
			if (CSpec[i][j][1] > max_element2){ max_element2 = CSpec[i][j][1];}
	    }
	    for (int i = 0; i < Z_LEN; i++) // Spectrum normalization
		{
            CSpec[i][j][0] = CSpec[i][j][0]/max_element1;
            CSpec[i][j][1] = CSpec[i][j][1]/max_element2;
		}
	    
		
    }
    // std::cout << Z_LEN << '\n';
    // std::cout << C_Ans[0][0][3] << '\n';


    // for (int i = 0+pos_shift; i < Z_LEN; i+=THREADS) //perturbative, polaron_shift, acoustic
    // {
    // 	std::cout << C_Ans[i][2][0] << '\n';
    // 	// std::cout << C_Ans[i][1][0] << '\n';
    // }
    // exit(0);
    // return 0;
}




void to_file(double Elo, std::string Filename)
{	
	
    double z, z_cm, arr_max;

	std::ofstream data;	data.open("/home/user/Physics/"+Filename+".csv");
	// Add headers to csv table
	data << "z_cm,";
	for (int j = 0; j < TEMP_LEN; ++j) // for TEMP_LEN temperatures 
	{
		data //<< "COpt_PertR"     << Temperatures[j] << ',' << "COpt_PertI"     << Temperatures[j] << ','
		     //<< "COpt_NonPertR"  << Temperatures[j] << ',' << "COpt_NonPertI"  << Temperatures[j] << ','
		     //<< "CAco_PertR"     << Temperatures[j] << ',' << "CAco_PertI"     << Temperatures[j] << ','
		     //<< "CAco_NonPertR"  << Temperatures[j] << ',' << "CAco_NonPertI"  << Temperatures[j] << ','
		     << "ESPEC_Pert"     << Temperatures[j] << ','
		     << "ESPEC_NonPert"  << Temperatures[j] << ',';	     
	}
	data << 0 << '\n' ; // just to have something at the end instead of ','

	// Add data to csv table
	for (int i = 0; i < Z_LEN; ++i)
	{
		z = ZSTART + i*STEP;
		z_cm = -(z+Elo)*8.05;
		data << z_cm << ',';
        for (int j = 0; j < TEMP_LEN; ++j) // for TEMP_LEN temperatures 
		{
			data //<< C_Ans[i][j][0].real() << ',' << C_Ans[i][j][0].imag() << ','
				 //<< C_Ans[i][j][1].real() << ',' << C_Ans[i][j][1].imag() << ','
			     //<< C_Ans[i][j][2].real() << ',' << C_Ans[i][j][2].imag() << ','
			     //<< C_Ans[i][j][3].real() << ',' << C_Ans[i][j][3].imag() << ','
			     << CSpec[i][j][0] << ',' 
			     << CSpec[i][j][1] << ',';
		}
		data << 0 << '\n'; // iust to have something at the end instead of ','
	}
}

};
using namespace workspace;


int main()
{
	//TESTS for lengths of arrays
	// for (int i = 0; i < Z_LEN; i++) 
    // {
    // 	std::cout << ZSTART + i*STEP << ' ';
    // }
    // std::cout << "\n\n\n";
    // for (int i = 0; i < Q_LEN; ++i) 
	// {
	// 	std::cout << QSTART + i*STEP << ' ';
	// }
	// std::cout << "\n\n\n";
	// for (int i = 0; i < TEMP_LEN; ++i) 
	// {
	// 	std::cout << Temperatures[i] << ' ';
	// }
	// std::cout << "\n\n\n";
	// for (int k = 0; k < CASES; k++)
	// {
	// std::cout << k << ' ';
	// }
	// std::cout << "\n\n\n";
	// for (int k = 0; k < SPECTRES; k++)
	// {
	// std::cout << k << ' ';
	// }
	// std::cout << "\n\n\n";
	// for (int k = 0; k < THREADS; k++)
	// {
	// std::cout << k << ' ';
	// }
	// std::cout << "\n\n\n";
	// simpsons1_();
	// std::cout << QSTART II QEND II Q_LEN <<"\n\n\n";
	// exit(0);

    double Shr,Ela,Elo;
    std::string Filename;

	Shr = 1.31;
    Ela = 9;// mEv
    Elo = 21.0773;// mEv
	Filename = "QD_PIB";
	ParC(Shr,Ela,Elo);
	to_file(Elo,Filename);

    Shr = 1.67;
    Ela = 9;// mEv
    Elo = 21.3253;// mEv
    Filename = "QD_Glass";
	ParC(Shr,Ela,Elo);
	to_file(Elo,Filename);

	Shr = 2.66;
    Ela = 9;// mEv
    Elo = 22.6891;// mEv
    Filename = "QD_Toluol";
	ParC(Shr,Ela,Elo);
	to_file(Elo,Filename);

        
return 0;
};
