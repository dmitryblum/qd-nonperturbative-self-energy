# QD Nonperturbative Self-energy



## Program recovers function C(z) using it's derivative dC(z)/dz

Here are client and server versions to parallel calcuations on multiple computers and multiple cores. You can rewrite it to work on single computer. 

C++ version of this program is uploaded here too.

## C++ version:

'''bash
g++ -O3 -pthread calculation.cpp  -o program
./program
'''
## Python version:

'''bash
python3 -m pip install numpy scipy pandas matplotlib functools
python3 calculation.py
'''

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

